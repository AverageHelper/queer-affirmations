/* eslint-disable import/no-default-export */

import { defineConfig } from "astro/config";
import cloudflare from "@astrojs/cloudflare";

// https://astro.build/config
export default defineConfig({
	root: ".", // Project root is the working directory
	outDir: "./dist", // Build to `/dist`
	publicDir: "./public", // Static assets live in `/public`
	srcDir: "./src", // Component sources live in `/src`
	output: "server", // Enable SSR
	adapter: cloudflare({
		mode: "directory", // Use a Cloudflare Pages function to handle requests
	}),
	build: {
		format: "file", // Build HTML pages at root, not in subdirectories
		assets: "assets", // Call the build assets folder "assets" instead of "_astro"
	},
	devToolbar: {
		enabled: false, // Don't show dev controls in the webpage
	},
	compressHTML: false, // Pretty output for nerds
});
