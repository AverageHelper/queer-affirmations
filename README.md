# queer-affirmations

A simple website that gives a random affirmation from a list, aimed at all the lovely queer beans out there.

Built with [Astro](https://astro.build) and TypeScript.

## Usage

To run the project locally, first make sure you have Node 20 or a newer LTS version.

Then, install the project dependencies with `npm ci` or `npm install`.

To run the project locally, use `npm start` for the bare Astro engine or `npm run preview` for the Cloudflare worker. (The behavior \*should be\* the same for both.)

## Contributing

This project lives primarily at [git.average.name](https://git.average.name/AverageHelper/queer-affirmations). Issues or pull requests should be filed there. You may sign in or create an account directly, or use one of several OAuth 2.0 providers from GitHub, GitLab, and others.
