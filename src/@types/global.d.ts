type NonEmptyArray<T> = [T, ...Array<T>];
type ReadonlyNonEmptyArray<T> = readonly [T, ...ReadonlyArray<T>];
