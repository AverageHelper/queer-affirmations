import type { ComponentProps } from "astro/types";
import type Emoji from "@/components/Emoji.astro";

type SupportedEmoji = ComponentProps<typeof Emoji>["name"];

interface Affirmation {
	/**
	 * A phrase that would fit the sentence "You {phrase}."
	 *
	 * @example
	 * secondPerson: "are awesome"
	 * `You ${secondPerson}` // "You are awesome"
	 */
	readonly secondPerson: string;

	/**
	 * A phrase that would fit the sentence "Every {phrase}."
	 *
	 * @example
	 * thirdPerson: "is awesome"
	 * `Everyone ${thirdPerson}` // "Everyone is awesome"
	 */
	readonly thirdPerson: string;

	/**
	 * An emoji that fits the phrase.
	 */
	readonly emoji: SupportedEmoji;
}

// TODO: Translate these into various languages
export const affirmations: ReadonlyNonEmptyArray<Affirmation> = [
	{ thirdPerson: "deserves love", secondPerson: "deserve love", emoji: "heart" },
	{
		thirdPerson: "has every reason to be proud",
		secondPerson: "have every reason to be proud",
		emoji: "blush",
	},
	{ thirdPerson: "is awesome", secondPerson: "are awesome", emoji: "star_struck" },
	{ thirdPerson: "is capable", secondPerson: "are capable", emoji: "superhero" },
	{ thirdPerson: "is cool", secondPerson: "are cool", emoji: "sunglasses" },
	{ thirdPerson: "is fierce", secondPerson: "are fierce", emoji: "fire" },
	{ thirdPerson: "is fun", secondPerson: "are fun", emoji: "partying_face" },
	{ thirdPerson: "is good", secondPerson: "are good", emoji: "thumbsup" },
	{ thirdPerson: "is great", secondPerson: "are great", emoji: "smile" },
	{ thirdPerson: "is kind", secondPerson: "are kind", emoji: "revolving_hearts" },
	{ thirdPerson: "is smart", secondPerson: "are smart", emoji: "brain" },
	{ thirdPerson: "is strong", secondPerson: "are strong", emoji: "muscle" },
	{ thirdPerson: "is talented", secondPerson: "are talented", emoji: "microphone" },
	{
		thirdPerson: "is thoughtful",
		secondPerson: "are thoughtful",
		emoji: "thought_balloon",
	},
	{
		thirdPerson: "is trying, and succeeding",
		secondPerson: "are trying, and succeeding",
		emoji: "fist",
	},
	{ thirdPerson: "smells great", secondPerson: "smell great", emoji: "smiling_face_with_3_hearts" },
];

export const lgbt = [
	"lesbian",
	"gay",
	"bisexual",
	"trans",
	"non-binary",
	"queer",
	"intersex",
	"asexual",
	"aromantic",
	"agender",
	"pansexual",
	"plural",
	"queer",
] as const;
